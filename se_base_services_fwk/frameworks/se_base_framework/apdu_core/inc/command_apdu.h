/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_INC_COMMAND_APDU_H
#define CORE_INC_COMMAND_APDU_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* the apdu command head as specified in ISO/IEC 7816-4 */
typedef struct ApduCommandHeader {
    uint8_t cla;
    uint8_t ins;
    uint8_t p1;
    uint8_t p2;
} ApduCommandHeader;

typedef struct CommandApdu CommandApdu;

CommandApdu *CreateCommandApdu(const ApduCommandHeader *header, const uint8_t *data, uint32_t dataLength,
    uint32_t expectedLength);

const uint8_t *GetApduCommandBuffer(const CommandApdu *commandApdu, uint32_t *bufferSize);

void DestroyCommandApdu(CommandApdu *commandApdu);

#ifdef __cplusplus
}
#endif

#endif // CORE_INC_COMMAND_APDU_H