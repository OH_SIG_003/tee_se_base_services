/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_INC_LOGGER_H
#define CORE_INC_LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __FILE_NAME__
#define LOG_FILE __FILE_NAME__
#else
#define LOG_FILE (__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define ARGS(fmt, ...) "[%s@%s:%d] " fmt, __FUNCTION__, LOG_FILE, __LINE__, ##__VA_ARGS__

void LogDebug(const char *fmt, ...) __attribute__((__format__(os_log, 1, 2)));
void LogInfo(const char *fmt, ...) __attribute__((__format__(os_log, 1, 2)));
void LogError(const char *fmt, ...) __attribute__((__format__(os_log, 1, 2)));

#define LOG_DEBUG(...) LogDebug(ARGS(__VA_ARGS__))
#define LOG_ERROR(...) LogError(ARGS(__VA_ARGS__))
#define LOG_INFO(...) LogInfo(ARGS(__VA_ARGS__))

typedef void LogWriter(const char *message);

typedef struct Logger {
    LogWriter *debug;
    LogWriter *error;
    LogWriter *info;
} Logger;

void SetApduLogger(const Logger *logger);

#ifdef __cplusplus
}
#endif

#endif // CORE_INC_LOGGER_H