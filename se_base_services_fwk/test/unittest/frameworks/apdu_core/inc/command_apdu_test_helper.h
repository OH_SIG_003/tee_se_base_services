/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_COMMAND_APDU_TEST_HELPER_H
#define TEST_COMMAND_APDU_TEST_HELPER_H

#include "command_apdu.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
class CommandApduTestHelper {
public:
    explicit CommandApduTestHelper(const ApduCommandHeader *header, const uint8_t *data, uint32_t dataLength,
        uint32_t expectedLength)
    {
        cmd_ = CreateCommandApdu(header, data, dataLength, expectedLength);
    }
    ~CommandApduTestHelper()
    {
        if (cmd_ != nullptr) {
            DestroyCommandApdu(cmd_);
            cmd_ = nullptr;
        }
    }
    CommandApdu *Get()
    {
        return cmd_;
    }

private:
    CommandApdu *cmd_ {nullptr};
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS

#endif // TEST_COMMAND_APDU_TEST_HELPER_H
