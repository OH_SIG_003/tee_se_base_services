add_library(test_helpers_obj OBJECT src/test_helpers.cpp)
target_include_directories(test_helpers_obj PUBLIC inc)
target_include_directories(test_helpers_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(test_helpers_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
