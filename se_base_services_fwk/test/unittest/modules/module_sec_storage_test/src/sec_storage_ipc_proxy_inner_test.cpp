/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <securec.h>
#include <vector>

#include "sec_storage_ipc_proxy_inner.h"
#include "test_helpers.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;
TEST(SecStorageIpcProxyInnerTest, SetFactoryResetAuthenticationKeyInputToBufferNullInput)
{
    {
        auto ret =
            SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, nullptr, nullptr, nullptr);
        EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
    }
    {
        StorageAuthKey key;
        auto ret =
            SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, &key, nullptr, nullptr);
        EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
    }
    {
        StorageAuthKey key;
        uint8_t buffer[1024] = {0};
        auto ret =
            SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, &key, buffer, nullptr);
        EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
    }
}

TEST(SecStorageIpcProxyInnerTest, SetFactoryResetAuthenticationKeyInputToBufferCaseBufferSizeIsNotEnough)
{
    uint8_t keyData[16] = {0};
    StorageAuthKey key = {.keyData = keyData, .keySize = sizeof(keyData)};
    uint8_t buffer[128] = {0};
    {
        uint32_t size = 0; // buffer size is 0, can not write anything
        auto ret = SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, &key, buffer, &size);
        EXPECT_EQ(ret, IPC_PARA_PROCESS_ERR_INDEX_1);
    }
    {
        uint32_t size = 4; // buffer size is 4, can only write one uint32
        auto ret = SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, &key, buffer, &size);
        EXPECT_EQ(ret, IPC_PARA_PROCESS_ERR_INDEX_2);
    }
    {
        uint32_t size = 8; // buffer size is 8, can only write 2 uint32
        auto ret = SetFactoryResetAuthenticationKeyInputToBuffer(LEVEL_USER_WIPE, ALGO_NIST_P256, &key, buffer, &size);
        EXPECT_EQ(ret, IPC_PARA_PROCESS_ERR_INDEX_3);
    }
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS