/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_INTERNEL_SE_API_MOCK_H
#define UNIT_TEST_INC_TEE_INTERNEL_SE_API_MOCK_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <gmock/gmock.h>

#include "function_mocker.h"

#include "tee_internal_se_api.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

class TeeInternalSeApiMock : public FunctionMocker<TeeInternalSeApiMock> {
public:
    using SeConfig = std::vector<std::pair<std::string, std::vector<std::vector<uint8_t>>>>;

    TeeInternalSeApiMock(const SeConfig &config = {});
    virtual ~TeeInternalSeApiMock() = default;

    MOCK_METHOD(TEE_Result, TEE_SEServiceGetReaders,
        (TEE_SEServiceHandle seServiceHandle, TEE_SEReaderHandle *seReaderHandleList, uint32_t *seReaderHandleListLen));

    MOCK_METHOD(TEE_Result, TEE_SEReaderGetName,
        (TEE_SEReaderHandle seReaderHandle, char *reader_name, uint32_t *readerNameLen));

    MOCK_METHOD(TEE_Result, TEE_SEReaderOpenSession,
        (TEE_SEReaderHandle seReaderHandle, TEE_SESessionHandle *seSessionHandle));

    MOCK_METHOD(void, TEE_SEReaderCloseSessions, (TEE_SEReaderHandle seReaderHandle));

    MOCK_METHOD(void, TEE_SESessionClose, (TEE_SESessionHandle seSessionHandle));

    MOCK_METHOD(void, TEE_SESessionCloseChannels, (TEE_SESessionHandle seSessionHandle));

    MOCK_METHOD(TEE_Result, TEE_SEServiceOpen, (TEE_SEServiceHandle * seServiceHandle));

    MOCK_METHOD(void, TEE_SEServiceClose, (TEE_SEServiceHandle seServiceHandle));

    MOCK_METHOD(TEE_Result, TEE_SEChannelTransmit,
        (TEE_SEChannelHandle seChannelHandle, void *command, uint32_t commandLen, void *response,
            uint32_t *responseLen));

    MOCK_METHOD(void, TEE_SEChannelClose, (TEE_SEChannelHandle seChannelHandle));

    MOCK_METHOD(TEE_Result, TEE_SESessionOpenLogicalChannel,
        (TEE_SESessionHandle seSessionHandle, TEE_SEAID *seAid, TEE_SEChannelHandle *seChannelHandle));

    MOCK_METHOD(TEE_Result, TEE_SESessionOpenBasicChannel,
        (TEE_SESessionHandle seSessionHandle, TEE_SEAID *seAid, TEE_SEChannelHandle *seChannelHandle));

    MOCK_METHOD(TEE_Result, TEE_SEChannelGetSelectResponse,
        (TEE_SEChannelHandle seChannelHandle, void *response, uint32_t *responseLen));

    MOCK_METHOD(TEE_Result, TEE_SESecureChannelOpen, (TEE_SEChannelHandle seChannelHandle, TEE_SC_Params *params));

    MOCK_METHOD(void, TEE_SESecureChannelClose, (TEE_SEChannelHandle seChannelHandle));

private:
    const SeConfig config_;
    void MockServiceOpenClose() const;
    void MockServiceReaders() const;
    void MockServiceReaderOpenCloseSession() const;
    void MockServiceChannelOpenCloseSession() const;
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS
#endif // UNIT_TEST_INC_TEE_INTERNEL_SE_API_MOCK_H